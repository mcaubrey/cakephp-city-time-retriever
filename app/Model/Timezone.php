<?php
class Timezone extends AppModel {
  
  var $name = 'Timezone';
  public $useTable = false; // Does not use database table.
  
  public $validate = array(
    'city' => array(
      'empty_validation' => array (
      'rule' => 'notBlank',
      'message' => 'City name must not be left empty'
      ),
      'city_name_validation' => array(
        'rule' => array('cityIsValid'),
        'message' => 'City name is invalid. Please enter one of the following cities: Montreal, Mumbai, New York, London, Paris or Sydney, Toronoto'
      ),
    )
  );
  
  public function cityIsValid($check) {
    
    $value = array_values($check);
    $value = $value[0];
    
    if (strcasecmp($value, "Montreal") == 0) {
      return true;
    }else if (strcasecmp($value, "Mumbai") == 0) {
        return true;
    }else if (strcasecmp($value, "New York") == 0) {
        return true;
    }else if (strcasecmp($value, "London") == 0) {
        return true;
    }else if (strcasecmp($value, "Paris") == 0) {
        return true;
    }else if (strcasecmp($value, "Sydney") == 0) {
        return true;
    }else if (strcasecmp($value, "Toronto") == 0) {
        return true;
    }else if (strcasecmp($value, "Akita") == 0) {
        return true;
    }
    
    return false;
  }
}
?>