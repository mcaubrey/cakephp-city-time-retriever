<h2>CakePHP City Time Retriever</h2>
<p>The form below allows you to enter the name of a city and get its current local time through AJAX using jQuery.</p>
<div id="cityUpdate"></div>
<?php
$data = $this->Js->get('#TimezoneForm')->serializeForm(array('isForm' => true, 'inline' => true));
$this->Js->get('#TimezoneForm')->event(
	  'submit',
	  $this->Js->request(
		array('controller' => 'timezones','action' => 'ajax_get_time'),
		array(
				'update' => '#cityUpdate',
				'data' => $data,
				'async' => true,    
				'dataExpression'=>true,
				'method' => 'POST',
				'before' => "$('#loading').fadeIn();$('#citySubmit').attr('disabled','disabled');",
				'complete' => "$('#loading').fadeOut();$('#citySubmit').removeAttr('disabled');",
			)
		)
	);
?>

<?php
echo $this->Form->create('Timezone', array(
	'id' => 'TimezoneForm',
	'controller' => 'timezones',
	'url' => 'ajax_get_time',
	'class' => '',
	'role' => 'form',
));
echo $this->Form->input('city', array('label' => 'Enter the city name', 'placeholder' => 'Enter the name of the city' )); 
echo $this->Form->submit('Get City Time', array('title' => 'Get Time', 'id'=>'citySubmit') );  
echo $this->Form->end();

?>
<div id="loading" style="display: none;"><div class="alert alert-info" role="alert"><i class=" fa fa-spinner fa-spin"></i> Please wait...</div></div>

<p>Enter any of the following cities in the text-field above to get their current local time: 
<ul>
	<li>London</li>
	<li>Montreal</li>
	<li>Mumbai</li>
  <li>Akita</li>
	<li>New York</li>
	<li>Paris</li>
	<li>Sydney</li>
	<li>Toronto</li>
</ul>
</p>
<hr>
<p style="font-size: .9em;color: #919191;">
  This is based on a tutorial for using AJAX forms with jQuery in CakePHP found on Mifty is Bored. You can see the original tutorial <a href="http://miftyisbored.com/a-complete-tutorial-on-cakephp-and-ajax-forms-using-jquery/">here </a> .
</p>
</p>