<?php

class TimezonesController extends AppController {
  public function beforeFilter() {
    parent::beforeFilter();
    
    if ($this->request->is('ajax')) {
      $this->layout = 'ajax';
    }
  }
  
  public function get_time() {
    
  }
  
  public function ajax_get_time() {
    $this->request->onlyAllow('ajax'); // No direct access via browser URL
    
    $content = '<div class="alert alert-warning" role="alert>Something unexpected happened.</div>';
    
    if ($this->request->is('post')) {
      
      $this->Timezone->set($this->request->data);
      if ($this->Timezone->validates()){
        $city = ucwords($this->request->data['Timezone']['city']);
        if (strcasecmp($city, "Montreal") == 0) {
          date_default_timezone_set('America/Toronto');
          $content = '<div class="alert alert-success" role="alert">The time in ' . $city . ' is <strong>' . date('D M d y - H:i:s ', time()) . '</strong></div>';
        }else if (strcasecmp($city, "Mumbai") == 0) {
          date_default_timezone_set('Asia/Calcutta');
          $content = '<div class="alert alert-success" role="alert">The time in ' . $city . ' is <strong>' . date('D M d y - H:i:s ', time()) . '</strong></div>';
        }else if (strcasecmp($city, "New York") == 0) {
          date_default_timezone_set('America/New_York');
          $content = '<div class="alert alert-success" role="alert">The time in ' . $city . ' is <strong>' . date('D M d y - H:i:s ', time()) . '</strong></div>';
        }else if (strcasecmp($city, "London") == 0) {
          date_default_timezone_set('Europe/London');
          $content = '<div class="alert alert-success" role="alert">The time in ' . $city . ' is <strong>' . date('D M d y - H:i:s ', time()) . '</strong></div>';
        }else if (strcasecmp($city, "Paris") == 0) {
          date_default_timezone_set('Europe/Paris');
          $content = '<div class="alert alert-success" role="alert">The time in ' . $city . ' is <strong>' . date('D M d y - H:i:s ', time()) . '</strong></div>';
        }else if (strcasecmp($city, "Akita") == 0) {
          date_default_timezone_set('Asia/Tokyo');
          $content = '<div class="alert alert-success" role="alert">The time in ' . $city . ' is <strong>' . date('D M d y - H:i:s ', time()) . '</strong></div>';
        }else if (strcasecmp($city, "Sydney") == 0) {
          date_default_timezone_set('Australia/Sydney');
          $content = '<div class="alert alert-success" role="alert">The time in ' . $city . ' is <strong>' . date('D M d y - H:i:s ', time()) . '</strong></div>';
        }else if (strcasecmp($city, "Toronto") == 0) {
          date_default_timezone_set('America/Toronto');
          $content = '<div class="alert alert-success" role="alert">The time in ' . $city . ' is <strong>' . date('D M d y - H:i:s ', time()) . '</strong></div>';
        }else {
          $content = '<div class="alert alert-warning" role="alert>Error: City not found. This should have been caught in validation.</div>';
        }
      }else {
        $errors = $this->Timezone->validationErrors;
        $flatErrors = Set::flatten($errors);
        if (count($errors) > 0) {
          $content = '<div class="alert alert-danger" role="alert">Could not get timezone. The following error(s) occured: ';
          $content .= '<ul>';
            foreach($flatErrors as $key => $value) {
              $content .= '<li><strong>' . $value . '</strong></li>';
            }
          $content .= '</ul>';
          $content .= '</div>';
        }
      }
    }
    
    $this->set(compact('content'));
    
    $this->render('ajax_response', 'ajax');
    
  }
}

?>